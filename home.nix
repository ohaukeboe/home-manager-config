{ config, pkgs, ... }:

{
  # Home Manager needs a bit of information about you and the paths it should
  # manage.
  home.username = "oskar";
  home.homeDirectory = "/home/oskar";

  # allowUnfree = true;

  programs.fish.enable = true;
  programs.bash.enable = true;

  # programs.bash.bashrcExtra = "[ ! -z '$PS1' ] && exec fish";

  # This value determines the Home Manager release that your configuration is
  # compatible with. This helps avoid breakage when a new Home Manager release
  # introduces backwards incompatible changes.
  #
  # You should not change this value, even if you update Home Manager. If you do
  # want to update the value, then make sure to first check the Home Manager
  # release notes.
  home.stateVersion = "22.11"; # Please read the comment before changing.

  services.lorri.enable = true;
  fonts.fontconfig.enable = true;

  nixpkgs.config.allowUnfree = true;
  # The home.packages option allows you to install Nix packages into your
  # environment.
  home.packages = with pkgs; [
    # # Adds the 'hello' command to your environment. It prints a friendly
    # # "Hello, world!" when run.
    # pkgs.hello

    # # It is sometimes useful to fine-tune packages, for example, by applying
    # # overrides. You can do that directly here, just don't forget the
    # # parentheses. Maybe you want to install Nerd Fonts with a limited number of
    # # fonts?
    # (pkgs.nerdfonts.override { fonts = [ "FantasqueSansMono" ]; })

    # # You can also create simple shell scripts directly inside your
    # # configuration. For example, this adds a command 'my-hello' to your
    # # environment:
    # (pkgs.writeShellScriptBin "my-hello" ''
    #   echo "Hello, ${config.home.username}!"
    # '')

    jetbrains.idea-community
    zotero

    neofetch
    tldr
    git

    # Emacs deps
    (python3.withPackages(ps: with ps; [
      matplotlib
      mypy
      python-lsp-server
      pytest
      nose
      black
      pyflakes
      pygments
    ]))

    direnv
    ripgrep
    fd
    pdf2svg
    texlive.combined.scheme-full
    plantuml
    texlab
    lua53Packages.digestif
    pandoc
    imagemagick
    exercism
    cmake
    gcc
    nixfmt
    valgrind
    gdb
    glibc
    man-pages
    maude
    julia
    rustup
    # rustc
    # cargo
    # rust-analyzer
    libtool
    gnumake
    shfmt
    shellcheck
    libvterm
    direnv
    elixir
    editorconfig-core-c
    sqlite
    openjdk
    gradle
    nodejs
    clang-tools
    glslang
    wakatime
    hunspell
    hunspellDicts.nb-no
    hunspellDicts.en_US
    haskell.compiler.ghc943
    haskell-language-server

    # Fonts
    nerdfonts
    powerline-fonts
    powerline-symbols
    roboto
    roboto-mono

  ];

    programs = {

      emacs = {
        enable = true;
        package = pkgs.emacs-gtk;
      };

      git = {
        enable = true;
        userName  = "ohaukeboe";
        userEmail = "ohaukeboe@pm.me";
      };

    };

  # Home Manager is pretty good at managing dotfiles. The primary way to manage
  # plain files is through 'home.file'.
  home.file = {
    # # Building this configuration will create a copy of 'dotfiles/screenrc' in
    # # the Nix store. Activating the configuration will then make '~/.screenrc' a
    # # symlink to the Nix store copy.
    # ".screenrc".source = dotfiles/screenrc;

    # # You can also set the file content immediately.
    # ".gradle/gradle.properties".text = ''
    #   org.gradle.console=verbose
    #   org.gradle.daemon.idletimeout=3600000
    # '';
  };

  # You can also manage environment variables but you will have to manually
  # source
  #
  #  ~/.nix-profile/etc/profile.d/hm-session-vars.sh
  #
  # or
  #
  #  /etc/profiles/per-user/oskar/etc/profile.d/hm-session-vars.sh
  #
  # if you don't want to manage your shell through Home Manager.
  home.sessionVariables = {
    # EDITOR = "emacs";
  };

  targets.genericLinux.enable = true;

  # Let Home Manager install and manage itself.
  programs.home-manager.enable = true;
}
